//
//  FocusTableView.swift
//  Contacts
//
//  Created by Tianyong Tang on 2018/10/30.
//  Copyright © 2018 Tianyong Tang. All rights reserved.
//

import UIKit

/**
 FocusTableView can be used to show items in focused way.
 Only one item can be focused in view center one at a time.

 It can reuse item views like UITableView.
 */
class FocusTableView: UIView {

    /**
     Layout direction.
     */
    enum Direction {

        /// Horizontal direction.
        /// Items will be placed in horizontal.
        case horizontal

        /// Vertical direction.
        /// Items will be placed in vertical.
        case vertical

    }

    /// The layout direction.
    var direction: Direction = .horizontal

    /// Data source which provides data to be shown.
    weak var dataSource: FocusTableViewDataSource?

    /// Delegate which receives and handles events of current view.
    weak var delegate: FocusTableViewDelegate?

    /// Number of items.
    private var numberOfItems = 0

    /// Size of each item.
    private var sizeOfItem: CGSize = .zero

    /// A table of visible cells indexed by item index.
    private var visibleCellTable: [Int: FocusTableViewCell] = [:]

    /// Reusable table view cells.
    private var reusableCells: [FocusTableViewCell] = []

    /// Last index range that is shown.
    private var lastIndexRange: CountableClosedRange<Int>? = nil

    /// Last focused table view cell.
    private var lastFocusedCell: FocusTableViewCell? = nil

    /// The scroll view used to scroll and focus items.
    private(set) lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()

        scrollView.delegate = self
        scrollView.clipsToBounds = false
        scrollView.contentInsetAdjustmentBehavior = .never

        // Add tap gesture recognizer.
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(scrollViewDidTap(_:)))
        tapGestureRecognizer.cancelsTouchesInView = false
        scrollView.addGestureRecognizer(tapGestureRecognizer)

        // When lazy loading, it will also be added to superview.
        addSubview(scrollView)

        // The scroll view will be placed in center.
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        scrollView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true

        return scrollView
    }()

    /// Width constraint of scroll view.
    private lazy var scrollViewWidthConstraint: NSLayoutConstraint = {
        let constraint = scrollView.widthAnchor.constraint(equalToConstant: 0)

        constraint.isActive = true

        return constraint
    }()

    /// Height constraint of scroll view.
    private lazy var scrollViewHeightConstraint: NSLayoutConstraint = {
        let constraint = scrollView.heightAnchor.constraint(equalToConstant: 0)

        constraint.isActive = true

        return constraint
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)

        initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        initialize()
    }

    /**
     Common initialization works.
     */
    private func initialize() {
        clipsToBounds = true

        // Improve drawing performance.
        isOpaque = true
        backgroundColor = .white
    }

    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let view = super.hitTest(point, with: event)

        // Forward touch event to scroll view.
        if view === self {
            return scrollView
        } else {
            return view
        }
    }

    /**
     Scroll view did tap.

     - parameter gestureRecognizer: The gesture recognizer.
     */
    @objc func scrollViewDidTap(_ gestureRecognizer: UITapGestureRecognizer) {
        guard numberOfItems > 0 else {
            return
        }

        var index: Int
        let location = gestureRecognizer.location(in: scrollView)

        switch direction {
        case .horizontal:
            index = Int(location.x / sizeOfItem.width)
        case .vertical:
            index = Int(location.y / sizeOfItem.height)
        }

        guard (0...numberOfItems).contains(index) else {
            return
        }

        scrollToIndex(index)
    }

    /**
     Scroll to index.

     - parameter index: The index that will scroll to.
     */
    func scrollToIndex(_ index: Int) {
        guard let cell = visibleCellAtIndex(index) else {
            return
        }

        var contentOffset: CGPoint

        switch direction {
        case .horizontal:
            contentOffset = CGPoint(x: CGFloat(index) * sizeOfItem.width, y: 0)
        case .vertical:
            contentOffset = CGPoint(x: 0, y: CGFloat(index) * sizeOfItem.height)
        }

        if let delegate = delegate {
            delegate.focusTableView(self, willSelectCell: cell, at: index)
        }

        scrollView.setContentOffset(contentOffset, animated: true)

        if let delegate = delegate {
            delegate.focusTableView(self, didSelectCell: cell, at: index)
        }
    }

    /**
     Get visible cell at index.

     - parameter index: The index of cell which you want to get.

     - returns: The visible cell, or nil if not found.
     */
    func visibleCellAtIndex(_ index: Int) -> FocusTableViewCell? {
        return visibleCellTable[index]
    }

    /**
     Dequeue a reusable cell.

     - returns: A reusable cell, or nil if not found.
     */
    func dequeueReusableCell() -> FocusTableViewCell? {
        return reusableCells.popLast()
    }

    /**
     Enqueue a reusable cell.

     - parameter cell: The cell which will be enqueued to reuse later.
     */
    private func enqueueReusableCell(_ cell: FocusTableViewCell) {
        // We assume that the queue is LIFO.
        reusableCells.append(cell)
    }

    /**
     Remove all retained cells.

     The retained cells include visible cells and invisible cells.
     */
    private func removeAllRetainedCells() {
        // Remove all visible cells.
        visibleCellTable.forEach { (_, cell) in
            cell.removeFromSuperview()
        }
        visibleCellTable.removeAll()

        // Remove all resuable cells, that is invisible cells.
        reusableCells.forEach { cell in
            cell.removeFromSuperview()
        }
        reusableCells.removeAll()
    }

    /**
     Reset scroll view.

     - parameter numberOfItems: The number of items.
     - parameter sizeOfItem: The size of a single item.
     */
    private func resetScrollView(numberOfItems: Int, sizeOfItem: CGSize) {
        // Set scroll view's size the same as a single item's.
        scrollViewWidthConstraint.constant = sizeOfItem.width
        scrollViewHeightConstraint.constant = sizeOfItem.height

        // Calculate content size of all items.
        // Items will be placed in horizontal by design.
        var contentSize: CGSize

        switch direction {
        case .horizontal:
            contentSize = CGSize(width: sizeOfItem.width * CGFloat(numberOfItems), height: sizeOfItem.height)
        case .vertical:
            contentSize = CGSize(width: sizeOfItem.width, height: sizeOfItem.height * CGFloat(numberOfItems))
        }

        scrollView.contentSize = contentSize

        // Always reset content offset, for simplicity.
        scrollView.contentOffset = .zero

        setNeedsLayout()
    }

    /**
     Reset state with data source.

     - parameter dataSource: The data source.
     */
    private func resetState(dataSource: FocusTableViewDataSource) {
        numberOfItems = dataSource.numberOfItems(focusTableView: self)
        sizeOfItem = dataSource.sizeOfItem(focusTableView: self)

        removeAllRetainedCells()

        resetScrollView(numberOfItems: numberOfItems, sizeOfItem: sizeOfItem)

        lastIndexRange = nil
        lastFocusedCell = nil
    }

    /**
     Load cells in given index range.

     - parameter range: The index range.
     */
    private func loadCells(range: CountableClosedRange<Int>) {
        guard let dataSource = dataSource else {
            return
        }

        /*
         Return if range not changed.
         This is a notable performance improvement.
         */
        if range == lastIndexRange {
            return
        }

        lastIndexRange = range

        /*
         Firstly, move the invisible cells to resuable queue.
         So that data source can reuse cell as soon as possible.
         */
        visibleCellTable.forEach { (index, cell) in
            if range.contains(index) {
                // The cell is visible and will be continue shown, nothing to do.
            } else {
                visibleCellTable.removeValue(forKey: index)
                enqueueReusableCell(cell)
            }
        }

        /*
         Finally, load cells for each index in range.
         */
        range.forEach { index in
            if let _ = visibleCellTable[index] {
                /* Cell at index is already exists, do nothing. */
                return
            }

            guard let cell = dataSource.focusTableView(self, cellAtIndex: index) else {
                return
            }

            visibleCellTable[index] = cell

            layoutCell(cell, at: index)
        }
    }

    /**
     Layout cell at index.

     - parameter cell: The cell to be layouted.
     - parameter index: The cell index.
     */
    private func layoutCell(_ cell: FocusTableViewCell, at index: Int) {
        var frame: CGRect

        let itemWidth = sizeOfItem.width
        let itemHeight = sizeOfItem.height

        switch direction {
        case .horizontal:
            frame = CGRect(x: itemWidth * CGFloat(index), y: 0, width: itemWidth, height: itemHeight)
        case .vertical:
            frame = CGRect(x: 0, y: itemHeight * CGFloat(index), width: itemWidth, height: itemHeight)
        }

        // Set frame directly, maybe Auto Layout is preferred.
        cell.frame = frame

        scrollView.addSubview(cell)
    }

    /**
     Load cells in specified range.

     - parameter rect: The content area of scroll view that needs to load cells.
     */
    private func loadCells(rect: CGRect) {
        guard numberOfItems > 0 else {
            return
        }

        var range: CountableClosedRange<Int>

        switch direction {
        case .horizontal:
            let minX = rect.minX
            let maxX = rect.maxX
            let itemWidth = sizeOfItem.width

            guard minX >= 0, maxX >= minX, itemWidth > 0 else {
                return
            }

            // Load an extra item for both sides.
            let startIndex = max(Int(minX / itemWidth) - 1, 0)
            let endIndex = min(Int(maxX / itemWidth) + 1, numberOfItems - 1)

            range = startIndex...endIndex
        case .vertical:
            let minY = rect.minY
            let maxY = rect.maxY
            let itemHeight = sizeOfItem.height

            guard minY >= 0, maxY >= minY, itemHeight > 0 else {
                return
            }

            // Load an extra item for both sides.
            let startIndex = max(Int(minY / itemHeight) - 1, 0)
            let endIndex = min(Int(maxY / itemHeight) + 1, numberOfItems - 1)

            range = startIndex...endIndex
        }

        loadCells(range: range)
    }

    /**
     Load cells in visible area.

     The visible area is scroll view's content area that clipped by table view.
     */
    private func loadCellsInVisibleArea() {
        let scrollViewOrigin = scrollView.frame.origin
        let scrollViewContentOffset = scrollView.contentOffset

        // Transform table view origin to scroll view's content area.
        let tableViewTransformedOrigin = CGPoint(
            x: scrollViewContentOffset.x - scrollViewOrigin.x,
            y: scrollViewContentOffset.y - scrollViewOrigin.y)

        let tableViewSize = frame.size
        let tableViewRect = CGRect(origin: tableViewTransformedOrigin, size: tableViewSize)

        let scrollViewContentSize = scrollView.contentSize
        let scrollViewContentRect = CGRect(origin: .zero, size: scrollViewContentSize)

        // The intersection is the clipped content area.
        let clippedRect = scrollViewContentRect.intersection(tableViewRect)

        // If clipped rect is malformed, return silently.
        if clippedRect.isNull || clippedRect.isEmpty || clippedRect.isInfinite {
            return
        }

        loadCells(rect: clippedRect)
    }

    /**
     Align content offset to page.

     - parameter contentOffset: The content offset to be aligned.

     - returns: An aligned content offset.
     */
    private func alignedContentOffset(_ contentOffset: CGPoint) -> CGPoint {
        switch direction {
        case .horizontal:
            let itemWidth = sizeOfItem.width

            guard itemWidth > 0 else {
                return .zero
            }

            let index = Int(contentOffset.x / itemWidth + 0.5)
            let contentOffset = CGPoint(x: CGFloat(index) * itemWidth, y: 0)

            return contentOffset
        case .vertical:
            let itemHeight = sizeOfItem.height

            guard itemHeight > 0 else {
                return .zero
            }

            let index = Int(contentOffset.y / itemHeight + 0.5)
            let contentOffset = CGPoint(x: 0, y: CGFloat(index) * itemHeight)

            return contentOffset
        }
    }

    /**
     Get current focused index.
     */
    private var currentFocusedIndex: Int? {
        var index: Int
        let contentOffset = scrollView.contentOffset

        switch direction {
        case .horizontal:
            let itemWidth = sizeOfItem.width

            guard itemWidth > 0 else {
                return nil
            }

            index = Int(contentOffset.x / itemWidth + 0.5)
        case .vertical:
            let itemHeight = sizeOfItem.height

            guard itemHeight > 0 else {
                return nil
            }

            index = Int(contentOffset.y / itemHeight + 0.5)
        }

        guard (0..<numberOfItems).contains(index) else {
            return nil
        }

        return index
    }

    /**
     Focus current cell and notify delegate.
     */
    private func focusCurrentCell() {
        guard
            let delegate = delegate,
            let currentFocusedIndex = currentFocusedIndex,
            let currentFocusedCell = visibleCellAtIndex(currentFocusedIndex),

            // If focused cell not changed, do nothing.
            currentFocusedCell !== lastFocusedCell
        else {
            return
        }

        if let lastFocusedCell = lastFocusedCell {
            delegate.focusTableView(self, didDefocusCell: lastFocusedCell)
        }

        lastFocusedCell = currentFocusedCell

        delegate.focusTableView(self, didFocusCell: currentFocusedCell)
    }

    /**
     Override it to reload data after subviews did layout.
     */
    override func layoutSubviews() {
        /* Controller may reload data before view did finish layout.
           In that case, table view's frame may be zero, there's no visible area. */
        reloadData()

        super.layoutSubviews()
    }

    /**
     Reload data from scratch.
     */
    func reloadData() {
        guard let dataSource = dataSource else {
            // Return silently instead of throwing exception.
            return
        }

        resetState(dataSource: dataSource)

        loadCellsInVisibleArea()

        focusCurrentCell()
    }

}

extension FocusTableView: UIScrollViewDelegate {

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        /*
         Loading cells in visible area when scroll view did scroll.
         */
        loadCellsInVisibleArea()

        if let delegate = delegate {
            delegate.focusTableViewDidScroll(self)
        }

        focusCurrentCell()
    }

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if let delegate = delegate {
            delegate.focusTableViewWillBeginDragging(self)
        }
    }

    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        targetContentOffset.pointee = alignedContentOffset(targetContentOffset.pointee)
    }

    func scrollViewShouldScrollToTop(_ scrollView: UIScrollView) -> Bool {
        if let delegate = delegate {
            return delegate.focusTableViewShouldScrollToTop(self)
        }

        return true
    }

}

/**
 FocusTableView data source protocol.
 */
protocol FocusTableViewDataSource: NSObjectProtocol {

    /**
     Get number of items that will be shown.

     - parameter focusTableView: The FocusTableView instance.

     - returns: Number of items.
     */
    func numberOfItems(focusTableView: FocusTableView) -> Int

    /**
     Get size of each item.

     Currently, multi-size is not supported.

     - parameter focusTableView: The FocusTableView instance.

     - returns: Number of items.
     */
    func sizeOfItem(focusTableView: FocusTableView) -> CGSize

    /**
     Get view to show item at given index.

     - parameter index: The index of item to be shown.

     - returns: A view to show item.
     */
    func focusTableView(_ focusTableView: FocusTableView, cellAtIndex index: Int) -> FocusTableViewCell?

}

/**
 FocusTableView delegate protocol.
 */
protocol FocusTableViewDelegate: NSObjectProtocol {

    /**
     Notify that some item will select.

     - parameter focusTableView: The table view.
     - parameter cell: The table view cell that will selected.
     - parameter index: The index that will selected.
     */
    func focusTableView(_ focusTableView: FocusTableView, willSelectCell cell: FocusTableViewCell, at index: Int)

    /**
     Notify that some item did select.

     - parameter focusTableView: The table view.
     - parameter cell: The table view cell that is selected.
     - parameter index: The index that is selected.
     */
    func focusTableView(_ focusTableView: FocusTableView, didSelectCell cell: FocusTableViewCell, at index: Int)

    /**
     Notify that some item did focus.

     - parameter focusTableView: The table view.
     - parameter cell: The table view cell that is focused.
     */
    func focusTableView(_ focusTableView: FocusTableView, didFocusCell cell: FocusTableViewCell)

    /**
     Notify that some item did defocus.

     - parameter focusTableView: The table view.
     - parameter cell: The table view cell that is defocused.
     */
    func focusTableView(_ focusTableView: FocusTableView, didDefocusCell cell: FocusTableViewCell)

    /**
     Notify that current table view will begin dragging.

     - parameter focusTableView: The table view that will begin dragging.
     */
    func focusTableViewWillBeginDragging(_ focusTableView: FocusTableView)

    /**
     Notify that current table view did scroll.

     - parameter focusTableView: The table view that is scrolling.
     */
    func focusTableViewDidScroll(_ focusTableView: FocusTableView)

    /**
     Ask that whether current table view should scroll to top or not.

     - parameter focusTableView: The table view that want to scroll to top or not.

     - returns: A boolean value which indicates whether current table view should scroll to top or not.
     */
    func focusTableViewShouldScrollToTop(_ focusTableView: FocusTableView) -> Bool

}
