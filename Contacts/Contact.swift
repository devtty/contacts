//
//  Contact.swift
//  Contacts
//
//  Created by Tianyong Tang on 2018/11/1.
//  Copyright © 2018 Tianyong Tang. All rights reserved.
//

import Foundation

/**
 A type represents a contact.
 */
class Contact {

    /**
     Contact error.
     */
    enum Panic: Error {

        /// Malformed JSON data.
        case malformedJSONData

    }

    /// The first name.
    let firstName: String

    /// The last name.
    let lastName: String

    /// The avatar file name.
    let avatarFileName: String

    /// The title.
    let title: String

    /// The introduction.
    let introduction: String

    /**
     Initialize contact with information.

     - parameter firstName: The first name.
     - parameter lastName: The last name.
     - parameter avatarFileName: The avatar file name.
     - parameter title: The title.
     - parameter introduction: The introduction.
     */
    init(
        firstName: String,
        lastName: String,
        avatarFileName: String,
        title: String,
        introduction: String)
    {
        self.firstName = firstName
        self.lastName = lastName
        self.avatarFileName = avatarFileName
        self.title = title
        self.introduction = introduction
    }

    /**
     Initialize contact with JSON object.

     - parameter jsonObject: The json object which contains contact information.
     */
    convenience init(jsonObject: Any) throws {
        guard
            let jsonObject = jsonObject as? [String: String],
            let firstName = jsonObject["first_name"],
            let lastName = jsonObject["last_name"],
            let avatarFileName = jsonObject["avatar_filename"],
            let title = jsonObject["title"],
            let introduction = jsonObject["introduction"]
        else {
            throw Panic.malformedJSONData
        }

        self.init(
            firstName: firstName,
            lastName: lastName,
            avatarFileName: avatarFileName,
            title: title,
            introduction: introduction)
    }

}
