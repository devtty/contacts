//
//  Interpolator.swift
//  Contacts
//
//  Created by Tianyong Tang on 2018/11/2.
//  Copyright © 2018 Tianyong Tang. All rights reserved.
//

import UIKit

/**
 Interpolator can be used to interpolate points on a curve.
 */
final class Interpolator {

    /**
     Interpolation method.
     */
    enum Method {

        /// Linear interpolation.
        /// The count is the number of interpolation point.
        case linear(count: Int)

    }

    private init() {
        /* Nop */
    }

    /// Shared interpolator.
    static let shared = Interpolator()

    /**
     Interpolate a line.

     - parameter from: The start of line.
     - parameter to: The end of line.
     - parameter method: The interpolation method.

     - returns: An array of values after interpolation.
     */
    func interpolate(from: CGFloat, to: CGFloat, method: Method) -> [CGFloat] {
        var points: [CGFloat] = []

        switch method {
        case .linear(let count):
            let steps = max(count, 0) + 1
            let stepLength = (to - from) / CGFloat(steps)

            for index in 0...steps {
                points.append(from + CGFloat(index) * stepLength)
            }
        }

        return points
    }

}
