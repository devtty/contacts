//
//  ContactsViewController.swift
//  Contacts
//
//  Created by Tianyong Tang on 2018/10/30.
//  Copyright © 2018 Tianyong Tang. All rights reserved.
//

import UIKit

/**
 The view controller for showing contacts.
 */
class ContactsViewController: ViewController {

    /// An array of contact.
    private var contacts: [Contact] = []

    /// Avatar table view.
    private lazy var avatarTableView: FocusTableView = {
        let tableView = FocusTableView()

        tableView.direction = .horizontal

        tableView.scrollView.showsHorizontalScrollIndicator = false
        tableView.scrollView.showsVerticalScrollIndicator = false

        tableView.dataSource = self
        tableView.delegate = self

        return tableView
    }()

    /// About table view.
    private lazy var aboutTableView: FocusTableView = {
        let tableView = FocusTableView()

        tableView.direction = .vertical
        tableView.scrollView.isPagingEnabled = true

        tableView.dataSource = self
        tableView.delegate = self

        return tableView
    }()

    /// Scroll shadow line.
    private lazy var scrollShadowLine = UIView()

    /// Scroll differential.
    /// It can synchronize velocity between two scroll view.
    private lazy var scrollDifferential: Differential<UIScrollView> = {
        let pair = Differential<UIScrollView>.Pair(
            left: avatarTableView.scrollView,
            right: aboutTableView.scrollView)

        let differential = Differential(pair: pair, transmissionOperation: { (pair, direction) in
            let avatarScrollView = pair.left
            let aboutScrollView = pair.right

            switch direction {
            case .none:
                break
            case .leftToRight:
                let aboutContentSize = aboutScrollView.contentSize
                let avatarContentSize = avatarScrollView.contentSize

                guard avatarContentSize.width > 0 else {
                    break
                }

                let scale = aboutContentSize.height / avatarContentSize.width

                let oldY = aboutScrollView.contentOffset.y
                let newY = scale * avatarScrollView.contentOffset.x

                /*
                 Interpolate animation to make scroll smooth.
                 Only interpolate animation when scroll slow down, it can save power.
                 */
                if abs(oldY - newY) < 10 {
                    let values = Interpolator.shared.interpolate(from: oldY, to: newY, method: .linear(count: Int(scale)))

                    for index in 0..<values.count {
                        UIView.animate(
                            withDuration: 0.01,
                            delay: TimeInterval(index) * 0.01,
                            usingSpringWithDamping: 1,
                            initialSpringVelocity: 1,
                            options: [.allowUserInteraction, .beginFromCurrentState, .curveLinear],
                            animations: {
                                aboutScrollView.setContentOffset(CGPoint(x: 0, y: values[index]), animated: false)
                            },
                            completion: nil
                        )
                    }
                } else {
                    aboutScrollView.setContentOffset(CGPoint(x: 0, y: newY), animated: false)
                }
            case .rightToLeft:
                avatarScrollView.setContentOffset(
                    CGPoint(x: (avatarScrollView.contentSize.width / aboutScrollView.contentSize.height) * aboutScrollView.contentOffset.y, y: 0),
                    animated: false
                )
            }
        })

        return differential
    }()

    /// Size of avatar table view cell.
    private let avatarTableViewCellSize = CGSize(width: 90, height: 90)

    override func viewDidLoad() {
        super.viewDidLoad()

        prepareSubviews()

        reloadContacts()
    }

    /**
     Reload contacts.
     */
    func reloadContacts() {
        do {
            contacts = try ContactBuilder.shared.build()

            avatarTableView.reloadData()
            aboutTableView.reloadData()
        } catch let error {
            // For simplicity, just print error.
            print(error.localizedDescription)
        }
    }

    /**
     Prepare subviews before show contacts.
     */
    private func prepareSubviews() {
        view.addSubview(aboutTableView)
        view.addSubview(scrollShadowLine)
        view.addSubview(avatarTableView)

        avatarTableView.translatesAutoresizingMaskIntoConstraints = false
        avatarTableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 18).isActive = true
        avatarTableView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor).isActive = true
        avatarTableView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor).isActive = true
        avatarTableView.heightAnchor.constraint(equalToConstant: avatarTableViewCellSize.height).isActive = true

        aboutTableView.translatesAutoresizingMaskIntoConstraints = false
        aboutTableView.topAnchor.constraint(equalTo: avatarTableView.bottomAnchor).isActive = true
        aboutTableView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor).isActive = true
        aboutTableView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor).isActive = true
        aboutTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true

        scrollShadowLine.translatesAutoresizingMaskIntoConstraints = false
        scrollShadowLine.topAnchor.constraint(equalTo: avatarTableView.bottomAnchor).isActive = true
        scrollShadowLine.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor).isActive = true
        scrollShadowLine.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor).isActive = true
        scrollShadowLine.heightAnchor.constraint(equalToConstant: 1).isActive = true

        scrollShadowLine.backgroundColor = .white
        scrollShadowLine.layer.shadowColor = UIColor.black.cgColor
        scrollShadowLine.layer.shadowOpacity = 0.75
        scrollShadowLine.layer.shadowRadius = 0
    }

    /**
     Set visibility of scroll shadow.
     */
    var isScrollShadowVisible: Bool = false {
        didSet {
            if isScrollShadowVisible == oldValue {
                return
            }

            let layer = scrollShadowLine.layer

            let keyPath = "shadowRadius"
            let animation = CABasicAnimation(keyPath: keyPath)

            let toValue: CGFloat = isScrollShadowVisible ? 4 : 0

            animation.duration = 0.3
            animation.fromValue = layer.shadowRadius
            animation.toValue = toValue

            layer.add(animation, forKey: keyPath)
            layer.shadowRadius = toValue
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension ContactsViewController: FocusTableViewDataSource {

    func numberOfItems(focusTableView: FocusTableView) -> Int {
        return contacts.count
    }

    func sizeOfItem(focusTableView: FocusTableView) -> CGSize {
        switch focusTableView {
        case avatarTableView:
            return avatarTableViewCellSize
        case aboutTableView:
            return aboutTableView.frame.size
        default:
            return .zero
        }
    }

    func focusTableView(_ focusTableView: FocusTableView, cellAtIndex index: Int) -> FocusTableViewCell? {
        switch focusTableView {
        case avatarTableView:
            return avatarTableViewCell(focusTableView: focusTableView, at: index)
        case aboutTableView:
            return aboutTableViewCell(focusTableView: focusTableView, at: index)
        default:
            return nil
        }
    }

    /**
     Return avatar table view cell.

     - parameter focusTableView: The table view.
     - parameter index: The index of cell.

     - returns: The avatar table view cell
     */
    private func avatarTableViewCell(focusTableView: FocusTableView, at index: Int) -> AvatarTableViewCell {
        var cell: AvatarTableViewCell

        if let reusableCell = focusTableView.dequeueReusableCell() as? AvatarTableViewCell {
            cell = reusableCell
        } else {
            cell = AvatarTableViewCell()
        }

        cell.contact = contacts[index]

        return cell
    }

    /**
     Return about table view cell.

     - parameter focusTableView: The table view.
     - parameter index: The index of cell.

     - returns: The about table view cell
     */
    private func aboutTableViewCell(focusTableView: FocusTableView, at index: Int) -> AboutTableViewCell {
        var cell: AboutTableViewCell

        if let reusableCell = focusTableView.dequeueReusableCell() as? AboutTableViewCell {
            cell = reusableCell
        } else {
            cell = AboutTableViewCell()
        }

        cell.contact = contacts[index]

        return cell
    }

}

extension ContactsViewController: FocusTableViewDelegate {

    private func updateTransmissionDirection(focusTableView: FocusTableView) {
        switch focusTableView {
        case avatarTableView:
            scrollDifferential.transmissionDirection = .leftToRight
        case aboutTableView:
            scrollDifferential.transmissionDirection = .rightToLeft
        default:
            break
        }
    }

    func focusTableView(_ focusTableView: FocusTableView, willSelectCell cell: FocusTableViewCell, at index: Int) {
        updateTransmissionDirection(focusTableView: focusTableView)
    }

    func focusTableView(_ focusTableView: FocusTableView, didSelectCell cell: FocusTableViewCell, at index: Int) {
        /* Nothing to do. */
    }

    func focusTableView(_ focusTableView: FocusTableView, didFocusCell cell: FocusTableViewCell) {
        switch focusTableView {
        case avatarTableView:
            if let cell = cell as? AvatarTableViewCell {
                cell.focus()
            }
        default:
            break
        }
    }

    func focusTableView(_ focusTableView: FocusTableView, didDefocusCell cell: FocusTableViewCell) {
        switch focusTableView {
        case avatarTableView:
            if let cell = cell as? AvatarTableViewCell {
                cell.defocus()
            }
        default:
            break
        }
    }

    func focusTableViewWillBeginDragging(_ focusTableView: FocusTableView) {
        updateTransmissionDirection(focusTableView: focusTableView)
    }

    func focusTableViewDidScroll(_ focusTableView: FocusTableView) {
        let scrollView = focusTableView.scrollView

        scrollDifferential.synchronize(from: scrollView)

        isScrollShadowVisible = true

        NSObject.cancelPreviousPerformRequests(withTarget: self)

        perform(
            #selector(focusTableViewDidEndScrolling(_:)),
            with: focusTableView,
            afterDelay: 0.3)
    }

    @objc func focusTableViewDidEndScrolling(_ focusTableView: FocusTableView) {
        isScrollShadowVisible = false
    }

    func focusTableViewShouldScrollToTop(_ focusTableView: FocusTableView) -> Bool {
        switch focusTableView {
        case avatarTableView:
            return false
        case aboutTableView:
            updateTransmissionDirection(focusTableView: focusTableView)
            return true
        default:
            return false
        }
    }

}
