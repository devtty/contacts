//
//  ContactBuilder.swift
//  Contacts
//
//  Created by Tianyong Tang on 2018/11/1.
//  Copyright © 2018 Tianyong Tang. All rights reserved.
//

import Foundation

/**
 Contacts builder.
 */
class ContactBuilder {

    /**
     Contact build error.
     */
    enum Panic: Error {

        /// JSON file not found.
        case fileNotFound

        /// Malformed JSON data.
        case malformedJSONData

    }

    private init() {
        /* Nop */
    }

    /// The shared contact builder.
    static let shared = ContactBuilder()

    /**
     Build contacts from local file.

     - returns: An array of contact.
     */
    func build() throws -> [Contact] {
        guard let contactsJSONFile = Bundle.main.url(forResource: "contacts", withExtension: "json") else {
            throw Panic.fileNotFound
        }

        let contactsJSON = try String(contentsOf: contactsJSONFile)

        guard let contactsJSONData = contactsJSON.data(using: .utf8) else {
            throw Panic.malformedJSONData
        }

        let contactsJSONObject = try JSONSerialization.jsonObject(with: contactsJSONData, options: [])

        guard let contactsJSONArray = contactsJSONObject as? [Any] else {
            throw Panic.malformedJSONData
        }

        let contacts = try contactsJSONArray.map { jsonObject in
            try Contact(jsonObject: jsonObject)
        }

        return contacts
    }

}
