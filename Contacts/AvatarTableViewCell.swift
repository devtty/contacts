//
//  AvatarTableViewCell.swift
//  Contacts
//
//  Created by Tianyong Tang on 2018/11/1.
//  Copyright © 2018 Tianyong Tang. All rights reserved.
//

import UIKit

/**
 A table view cell used to show avatar.
 */
class AvatarTableViewCell: FocusTableViewCell {

    /// The contact to show.
    var contact: Contact? {
        didSet {
            renderData()
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        initialize()
    }

    /**
     Common initialization works.
     */
    private func initialize() {
        // Improve drawing performance.
        isOpaque = true
        backgroundColor = .white
    }

    /// The focus border layer.
    private lazy var focusBorderLayer: CAShapeLayer = {
        let layer = CAShapeLayer()

        // Hard code the style.
        layer.lineWidth = 4
        layer.strokeColor = UIColor(red: 0.78, green: 0.87, blue: 0.96, alpha: 1).cgColor
        layer.fillColor = nil

        return layer
    }()

    /// Image view used to show avatar.
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()

        addSubview(imageView)

        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        imageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true

        return imageView
    }()

    /**
     Render data, that is to show avatar.
     */
    private func renderData() {
        guard let contact = contact else {
            /* If contact is nil, return silently. */
            return
        }

        let avatarImage = UIImage(named: contact.avatarFileName)
        imageView.image = avatarImage
        imageView.sizeToFit()
    }

    /**
     Focus avatar. It will add border around avatar.
     */
    func focus() {
        guard let image = imageView.image else {
            return
        }

        let rect = CGRect(origin: .zero, size: image.size)
        let path = CGPath(ellipseIn: rect, transform: nil)

        focusBorderLayer.path = path
        imageView.layer.addSublayer(focusBorderLayer)
    }

    /**
     Defocus avatar. It will remove border around avatar.
     */
    func defocus() {
        focusBorderLayer.removeFromSuperlayer()
    }

}
