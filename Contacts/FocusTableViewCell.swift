//
//  FocusTableViewCell.swift
//  Contacts
//
//  Created by Tianyong Tang on 2018/10/31.
//  Copyright © 2018 Tianyong Tang. All rights reserved.
//

import UIKit

/**
 The cell used to show item in FocusTableView.

 For simplicity, it has no reuse identifier like UITableViewCell.
 */
class FocusTableViewCell: UIView {

    // TODO: Implementation

}
