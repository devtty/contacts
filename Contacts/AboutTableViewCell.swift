//
//  AboutTableViewCell.swift
//  Contacts
//
//  Created by Tianyong Tang on 2018/11/1.
//  Copyright © 2018 Tianyong Tang. All rights reserved.
//

import UIKit

/**
 A table view cell used to show about me.
 */
class AboutTableViewCell: FocusTableViewCell {

    /// The contact to show.
    var contact: Contact? {
        didSet {
            renderData()
        }
    }

    // MARK: Components

    /// The name label.
    private let nameLabel = UILabel()

    /// The title label.
    private let titleLabel = UILabel()

    /// The introduction head label.
    private let introductionHeadLabel = UILabel()

    /// The introduction body text view.
    private let introductionBodyTextView = UITextView()

    // MARK: Component Styles

    /*
     Maybe dynamic font type is preferred.
     */

    /// The name font.
    private let nameFont = UIFont.systemFont(ofSize: 19)

    /// The first name font.
    private let firstNameFont = UIFont.boldSystemFont(ofSize: 19)

    /// The title font.
    private let titleFont = UIFont.systemFont(ofSize: UIFont.systemFontSize)

    /// The introduction head font.
    private let introductionHeadFont = UIFont.systemFont(ofSize: UIFont.systemFontSize, weight: .bold)

    /// The introduction body font.
    private let introductionBodyFont = UIFont.systemFont(ofSize: UIFont.systemFontSize, weight: .light)

    /// The title font.
    private let titleColor = UIColor.gray

    /// The introduction body font.
    private let introductionBodyColor = UIColor.gray

    // MARK: Initialization

    override init(frame: CGRect) {
        super.init(frame: frame)

        initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        initialize()
    }

    /**
     Common initialization works.
     */
    private func initialize() {
        // Improve drawing performance.
        isOpaque = true
        backgroundColor = .white

        /*
         Set up style for components.
         */
        nameLabel.font = nameFont
        nameLabel.textAlignment = .center

        titleLabel.font = titleFont
        titleLabel.textColor = titleColor
        titleLabel.textAlignment = .center

        introductionHeadLabel.font = introductionHeadFont
        introductionHeadLabel.text = NSLocalizedString("About me", comment: "Contact introduction head title")

        introductionBodyTextView.font = introductionBodyFont
        introductionBodyTextView.textColor = introductionBodyColor
        introductionBodyTextView.isEditable = false

        /*
         Set up layout for subviews.
         */

        let subviews = [
            nameLabel,
            titleLabel,
            introductionHeadLabel,
            introductionBodyTextView
        ]

        subviews.forEach { subview in
            addSubview(subview)
            subview.translatesAutoresizingMaskIntoConstraints = false

            // Improve drawing performance.
            subview.isOpaque = true
            subview.backgroundColor = .white
        }

        /*
         There are some magic number below, but let it be.
         */

        nameLabel.topAnchor.constraint(equalTo: topAnchor, constant: 20).isActive = true
        nameLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 15).isActive = true
        nameLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -15).isActive = true
        nameLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true

        titleLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor).isActive = true
        titleLabel.leftAnchor.constraint(equalTo: nameLabel.leftAnchor).isActive = true
        titleLabel.rightAnchor.constraint(equalTo: nameLabel.rightAnchor).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true

        introductionHeadLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 30).isActive = true
        introductionHeadLabel.leftAnchor.constraint(equalTo: nameLabel.leftAnchor, constant: 5).isActive = true
        introductionHeadLabel.rightAnchor.constraint(equalTo: nameLabel.rightAnchor).isActive = true
        introductionHeadLabel.heightAnchor.constraint(equalToConstant: 14).isActive = true

        introductionBodyTextView.topAnchor.constraint(equalTo: introductionHeadLabel.bottomAnchor).isActive = true
        introductionBodyTextView.leftAnchor.constraint(equalTo: nameLabel.leftAnchor).isActive = true
        introductionBodyTextView.rightAnchor.constraint(equalTo: nameLabel.rightAnchor).isActive = true
        introductionBodyTextView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }

    // MARK: Render Data

    /**
     Render data, that is to show contact information.
     */
    private func renderData() {
        guard let contact = contact else {
            return
        }

        // Set contact name.
        let plainFullName = "\(contact.firstName) \(contact.lastName)"
        let attributedFullName = NSMutableAttributedString(string: plainFullName)

        attributedFullName.addAttribute(.font, value: firstNameFont, range: NSMakeRange(0, contact.firstName.count))
        nameLabel.attributedText = attributedFullName

        // Set contact title.
        titleLabel.text = contact.title

        // Set introduction body.
        introductionBodyTextView.text = contact.introduction
    }

}
