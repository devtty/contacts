//
//  Differential.swift
//  Contacts
//
//  Created by Tianyong Tang on 2018/11/1.
//  Copyright © 2018 Tianyong Tang. All rights reserved.
//

import UIKit

/**
 The differential of two objects.

 Inspired by car differential, I plan to create a type to synchronize two objects whose state have differential.
 */
final class Differential<Object: Equatable> {

    /**
     The object pair.
     */
    struct Pair {

        /// Left object, which like the left wheel of car.
        let left: Object

        /// Right object, which like the right wheel of car.
        let right: Object

    }

    /**
     Transmission direction.
     */
    enum TransmissionDirection {

        /// The state will not transmit.
        case none

        /// The state will transmit from left to right.
        case leftToRight

        /// The state will transmit from right to left.
        case rightToLeft

    }

    /// The transmission direction.
    var transmissionDirection: TransmissionDirection = .none

    /// Type of transmission operation.
    typealias TransmissionOperation = (Pair, TransmissionDirection) -> Void

    /// The object pair.
    let pair: Pair

    //// The transmission operation.
    let transmissionOperation: TransmissionOperation

    /**
     Initialize differential.

     - parameter pair: The object pair.
     - parameter transmissionOperation: The transmission operation.
     */
    init(pair: Pair, transmissionOperation: @escaping TransmissionOperation) {
        self.pair = pair
        self.transmissionOperation = transmissionOperation
    }

    /// The leader object.
    private var leaderObject: Object? {
        switch transmissionDirection {
        case .none:
            return nil
        case .leftToRight:
            return pair.left
        case .rightToLeft:
            return pair.right
        }
    }

    /**
     Synchronize state by performing transmission operation.

     - parameter from: The object from which the synchronization will start.
     */
    func synchronize(from: Object) {
        guard from == leaderObject else {
            return
        }
        transmissionOperation(pair, transmissionDirection)
    }

}
